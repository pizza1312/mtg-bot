#!/usr/bin/env python3

from mtgjson import Mtgjson, Card
from os import listdir
import json
import requests
from time import sleep, time
from math import floor
import signal


done = 0

def handler(signum, frame):
    global done
    print("Exiting after saving "+str(done)+" card images")
    exit(1)

signal.signal(signal.SIGINT, handler)


def save_image(url, filename):
    with open("images/"+filename, "wb") as file:
        response = requests.get(url)
        file.write(response.content)



mtg = Mtgjson()

mtg.data = None
mtg.image_index = None
mtg.name_index = None

#print(mtg.get_card_by_name("Stroke of Genius").image)


total_length = len(mtg.card_index)
json_output = {}
i = 0

dir = listdir("images")
existing_files = len(dir)

start = time()
print("found "+str(existing_files)+" cached files,",total_length-existing_files,"to go.")
print("saving cards...")
for card in mtg.card_index.values():
    i += 1
    if card.uuid+".jpg" in dir:
        continue

    if type(card.image) == str:
        # single image
        save_image(card.image, card.uuid+".jpg")
    else:
        save_image(card.image[0], card.uuid+"jpg")
        save_image(card.image[1], card.uuid+"_b.jpg")

    done += 1
    print(".",end="", flush=True)

    if i%10==0:
        print("")
        time_so_far = time()-start
        time_per_card = time_so_far / done
        cards_remaining = total_length-i
        est_time_remaining = time_per_card * cards_remaining
        str_time_remaining = str(floor(est_time_remaining/3600))+" hours "+str(floor((est_time_remaining%3600)/60))+" minutes ("+str(time_per_card)+" per card)"
        print(round(i*100/total_length),"% - estimated time remaining:",str_time_remaining)


    # sleep
    sleep(1)

print("Done! saved",i,"cards")
