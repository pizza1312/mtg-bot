#!/usr/bin/env python3

import json
import random
#import pickle
import time
from os import listdir

#print(Set.where(name="khans of tarkir").all()[0].booster)
#get_mythic_rares("ALA")

#cards = Card.where(name="Archangel Avacyn").all()
#for card in cards:
#    print(card.name +" | "+ card.rarity)

def melt_dicts(list_of_dicts, *values):
    output={}

    for v in values:
        output[v] = []

    for d in list_of_dicts:
        for v in values:
            output[v] += [d.get(v)]

    return output

class Mtgjson(object):
    """docstring for Mtgjson."""

    COLORS = ["W", "U", "B", "R", "G"]
    SETS = ["10E", "2ED", "2XM", "3ED", "4BB", "4ED", "5DN", "5ED", "6ED", "7ED", "8ED", "9ED", "A25", "AAFR", "AER", "AFC", "AFR", "AJMP", "AKH", "AKHM", "AKR", "ALA", "ALL", "AMH1", "AMH2", "AMID", "ANA", "ANB", "APC", "ARB", "ARC", "ARN", "ASTX", "ATH", "ATQ", "AVOW", "AVR", "AZNR", "BBD", "BFZ", "BNG", "BOK", "BRB", "BTD", "C13", "C14", "C15", "C16", "C17", "C18", "C19", "C20", "C21", "CC1", "CC2", "CED", "CEI", "CHK", "CHR", "CM1", "CM2", "CMA", "CMB1", "CMB2", "CMD", "CMR", "CN2", "CNS", "CON", "CP1", "CP2", "CP3", "CSP", "CST", "DBL", "DD1", "DD2", "DDC", "DDD", "DDE", "DDF", "DDG", "DDH", "DDI", "DDJ", "DDK", "DDL", "DDM", "DDN", "DDO", "DDP", "DDQ", "DDR", "DDS", "DDT", "DDU", "DGM", "DIS", "DKA", "DKM", "DOM", "DPA", "DRB", "DRK", "DST", "DTK", "DVD", "E01", "E02", "ELD", "EMA", "EMN", "EVE", "EVG", "EXO", "EXP", "F01", "F02", "F03", "F04", "F05", "F06", "F07", "F08", "F09", "F10", "F11", "F12", "F13", "F14", "F15", "F16", "F17", "F18", "FBB", "FEM", "FJMP", "FMB1", "FNM", "FRF", "FUT", "G00", "G01", "G02", "G03", "G04", "G05", "G06", "G07", "G08", "G09", "G10", "G11", "G17", "G18", "G99", "GK1", "GK2", "GN2", "GNT", "GPT", "GRN", "GS1", "GTC", "GVL", "H09", "H17", "H1R", "HA1", "HA2", "HA3", "HA4", "HA5", "HHO", "HML", "HOP", "HOU", "HTR16", "HTR17", "HTR18", "HTR19", "HTR20", "ICE", "IKO", "IMA", "INV", "ISD", "ITP", "J12", "J13", "J14", "J15", "J16", "J17", "J18", "J19", "J20", "J21", "JGP", "JMP", "JOU", "JUD", "JVC", "KHC", "KHM", "KLD", "KLR", "KTK", "L12", "L13", "L14", "L15", "L16", "L17", "LEA", "LEB", "LEG", "LGN", "LRW", "M10", "M11", "M12", "M13", "M14", "M15", "M19", "M20", "M21", "MAFR", "MB1", "MBS", "MD1", "ME1", "ME2", "ME3", "ME4", "MED", "MGB", "MH1", "MH2", "MIC", "MID", "MIR", "MKHM", "MM2", "MM3", "MMA", "MMH2", "MMQ", "MOR", "MP2", "MPR", "MPS", "MRD", "MSTX", "MZNR", "NEC", "NEM", "NEO", "NPH", "OAFC", "OANA", "OARC", "OC13", "OC14", "OC15", "OC16", "OC17", "OC18", "OC19", "OC20", "OC21", "OCM1", "OCMD", "ODY", "OE01", "OGW", "OHOP", "OLGC", "OMIC", "ONS", "OPC2", "OPCA", "ORI", "OVNT", "OVOC", "P02", "P03", "P04", "P05", "P06", "P07", "P08", "P09", "P10", "P10E", "P11", "P15A", "P2HG", "P5DN", "P8ED", "P9ED", "PAER", "PAFR", "PAKH", "PAL00", "PAL01", "PAL02", "PAL03", "PAL04", "PAL05", "PAL06", "PAL99", "PALA", "PALP", "PANA", "PAPC", "PARB", "PARC", "PARL", "PAST", "PAVR", "PBBD", "PBFZ", "PBNG", "PBOK", "PBOOK", "PC2", "PC20", "PCA", "PCEL", "PCHK", "PCMD", "PCMP", "PCNS", "PCON", "PCSP", "PCY", "PD2", "PD3", "PDGM", "PDIS", "PDKA", "PDOM", "PDP10", "PDP12", "PDP13", "PDP14", "PDP15", "PDRC", "PDST", "PDTK", "PDTP", "PELD", "PELP", "PEMN", "PEVE", "PEXO", "PF19", "PF20", "PF21", "PFRF", "PFUT", "PG07", "PG08", "PGPT", "PGPX", "PGRN", "PGRU", "PGTC", "PGTW", "PHEL", "PHJ", "PHOP", "PHOU", "PHPR", "PHUK", "PI13", "PI14", "PIDW", "PIKO", "PINV", "PISD", "PJ21", "PJAS", "PJJT", "PJOU", "PJSE", "PJUD", "PKHM", "PKLD", "PKTK", "PL21", "PLC", "PLG20", "PLG21", "PLGM", "PLGN", "PLIST", "PLNY", "PLRW", "PLS", "PM10", "PM11", "PM12", "PM13", "PM14", "PM15", "PM19", "PM20", "PM21", "PMBS", "PMEI", "PMH1", "PMH2", "PMIC", "PMID", "PMMQ", "PMOA", "PMOR", "PMPS", "PMPS06", "PMPS07", "PMPS08", "PMPS09", "PMPS10", "PMPS11", "PMRD", "PNAT", "PNEM", "PNPH", "PODY", "POGW", "PONS", "POR", "PORI", "PPC1", "PPCY", "PPLC", "PPLS", "PPOD", "PPP1", "PPRO", "PPTK", "PR2", "PRAV", "PRED", "PRES", "PRIX", "PRM", "PRNA", "PROE", "PRTR", "PRW2", "PRWK", "PS11", "PS14", "PS15", "PS16", "PS17", "PS18", "PS19", "PSAL", "PSCG", "PSDC", "PSDG", "PSHM", "PSOI", "PSOK", "PSOM", "PSS1", "PSS2", "PSS3", "PSTH", "PSTX", "PSUM", "PSUS", "PTC", "PTG", "PTHB", "PTHS", "PTK", "PTKDF", "PTMP", "PTOR", "PTSP", "PUDS", "PULG", "PUMA", "PUNH", "PURL", "PUSG", "PUST", "PVAN", "PVOW", "PW09", "PW10", "PW11", "PW12", "PW21", "PW22", "PWAR", "PWCQ", "PWOR", "PWOS", "PWPN", "PWWK", "PXLN", "PXTC", "PZ1", "PZ2", "PZEN", "PZNR", "Q06", "RAV", "REN", "RIN", "RIX", "RNA", "ROE", "RQS", "RTR", "S00", "S99", "SCG", "SHM", "SLD", "SLU", "SMID", "SOI", "SOK", "SOM", "SS1", "SS2", "SS3", "STA", "STH", "STX", "SUM", "SZNR", "TBTH", "TD0", "TD2", "TDAG", "TFTH", "THB", "THP1", "THP2", "THP3", "THS", "TMP", "TOR", "TPR", "TSB", "TSP", "TSR", "UDS", "UGIN", "UGL", "ULG", "UMA", "UND", "UNF", "UNH", "USG", "UST", "V09", "V10", "V11", "V12", "V13", "V14", "V15", "V16", "V17", "VIS", "VMA", "VOC", "VOW", "W16", "W17", "WAR", "WC00", "WC01", "WC02", "WC03", "WC04", "WC97", "WC98", "WC99", "WTH", "WWK", "XANA", "XLN", "Y22", "ZEN", "ZNC", "ZNE", "ZNR"]
    SCRYFALL_IMAGE_PLACEHOLDER = "https://c2.scryfall.com/file/scryfall-errors/soon.jpg"

    def __init__(self, json_filename="AllPrintings.json"):
        print("Initializing...")
        super(Mtgjson, self).__init__()
        self.json_filename = json_filename
        self.load_data()
        print("MTG JSON loaded")
        self.colors = ["W", "U", "B", "R", "G"]
        self.card_index = {}
        self.image_index = {}
        self.name_index = Trie()
        self.cards_are_indexed = False
        self.images_are_indexed = False
        self.images_linked = False
        print("Indexing cards...")
        self.index_cards()
        print("Linking images...")
        self.index_scryfall_data()
        self.link_images_to_cards()
        print("Ready")

    def load_data(self):
        with open(self.json_filename) as json_file:
            self.data = json.load(json_file)

    def index_cards(self):
        """process every single card, get its uuid, put it in a hash table"""
        for set in Mtgjson.SETS:
            for c in self.get_set(set)["cards"]:
                card = Card(json_dict=c)
                card.set = set
                self.card_index[card.uuid] = card
                self.name_index[card.name] = card

        self.cards_are_indexed = True

    def index_scryfall_data(self):
        scryfall_filename = ""

        for filename in listdir():
            if "default-cards" in filename and ".json" in filename:
                scryfall_filename = filename
                break

        if scryfall_filename == "":
            print("Can't find scryfall json file, skipping image indexing")

        with open(scryfall_filename) as json_file:
            data = json.load(json_file)

        self.image_index = {}
        for card in data:

            faces = card.get("card_faces")
            images = card.get("image_uris")

            if images is not None:
                self.image_index[card["id"]] = images["normal"]
            elif faces is not None:
                if card["image_status"] == "missing":
                    self.image_index[card["id"]] = [Mtgjson.SCRYFALL_IMAGE_PLACEHOLDER for f in faces]
                else:
                    self.image_index[card["id"]] = [f["image_uris"]["normal"] for f in faces]
            else:
                self.image_index[card["id"]] = Mtgjson.SCRYFALL_IMAGE_PLACEHOLDER

        self.images_are_indexed = True

    def link_images_to_cards(self):
        if not self.cards_are_indexed:
            self.index_cards()
        if not self.cards_are_indexed:
            print("Cannot link images (cards not indexed)")
            return
        if not self.images_are_indexed:
            self.index_scryfall_data()
        if not self.images_are_indexed:
            print("Cannot link images (Scryfall data not indexed)")
            return
        #Okay, checks out of the way, let's do this

        for card in self.card_index.values():
            scryfall_id = card.identifiers["scryfallId"]
            card.image = self.image_index[scryfall_id]

        self.images_linked = True

    def get_set(self, set_code):
        return self.data["data"][set_code.upper()]

    def get_card_by_uuid(self, uuid, set_code=None, only_that_set=False):
        #print("Looking for card ["+uuid+"]...")

        if self.cards_are_indexed:
            card = self.card_index.get(uuid)
            if card is not None:
                return card
            else:
                print("Card not indexed, searching json data")

        if set_code is not None:
            sets_to_search = [set_code]
            if not only_that_set:
                sets_to_search += Mtgjson.SETS
        else:
            sets_to_search = Mtgjson.SETS

        for set in sets_to_search:
            for c in self.get_set(set)["cards"]:
                if c["uuid"]==uuid:
                    #print("Got card: "+c["name"])
                    card = Card(json_dict=c)
                    card.set = set
                    return card

        if not only_that_set:
            print("Card "+uuid+" not found")
        else:
            print("Card "+uuid+" not found in set "+set_code)
        return None

    def get_cards_by_uuids(self, uuids, set_code=None, only_that_set=False):
        # much better for multiple cards, loops through cards way less
        uuidlist = list(uuids)
        requested_amount = len(uuidlist)

        output = []

        if self.cards_are_indexed:
            for uuid in uuids:
                card = self.card_index.get(uuid)
                if card is not None:
                    output += [card]
                    uuidlist.remove(uuid)
                else:
                    print("failed to find card",uuid)

            if len(output) == requested_amount and len(uuidlist) == 0:
                return output
            else:
                print("Requested",requested_amount,", got",len(output),"(uuids left",len(uuidlist),")")
                print("Not all cards found in index, searching json")


        if set_code is not None:
            sets_to_search = [set_code]
            if not only_that_set:
                sets_to_search += Mtgjson.SETS
        else:
            sets_to_search = Mtgjson.SETS

        for set in sets_to_search:
            for card in self.get_set(set)["cards"]:
                if card["uuid"] in uuidlist:
                    #print("Got card: "+c["name"])
                    c = Card(json_dict=card)
                    c.set = set
                    output += [c]
                    uuidlist.remove(card["uuid"])

        if len(output) < requested_amount:
            print("Could not find all cards (requested "+str(requested_amount)+", found "+str(len(output)))
            # find missing cards, for the craic
            uuidlist = list(uuids)
            for card in output:
                uuidlist.remove(card.uuid)
            print("Failed to find: "+str(uuidlist))


        return output


    def get_card_by_name(self, name):
        if not self.cards_are_indexed:
            self.index_cards()

        return self.name_index[name][0]

    def get_cards_by_name(self, name):
        if not self.cards_are_indexed:
            self.index_cards()

        cards = self.name_index[name]
        if cards is not None:
            return cards

        keys = self.name_index.get_keys_with_prefix(name)
        cards = []
        for k in keys:

            cards += self.name_index[k]
        return cards

    def uuid_search(self, uuid, set_code=None, only_that_set=False):
        #print("Looking for card ["+uuid+"]...")

        if set_code is not None:
            sets_to_search = [set_code]
            if not only_that_set:
                sets_to_search += Mtgjson.SETS
        else:
            sets_to_search = Mtgjson.SETS

        for set in sets_to_search:
            for c in self.get_set(set)["cards"]:
                if uuid in c["uuid"]:
                    #print("Got card: "+c["name"])
                    card = Card(json_dict=c)
                    card.set = set
                    return card

        if not only_that_set:
            print("Card with "+uuid+" in uuid not found")
        else:
            print("Card with "+uuid+" in uuid not found in set "+set_code)
        return None


    def get_token(self, uuid, set_code=None, only_that_set=False):

        if set_code is not None:
            sets_to_search = [set_code]
            if not only_that_set:
                sets_to_search += Mtgjson.SETS
        else:
            sets_to_search = Mtgjson.SETS

        for set in sets_to_search:
            set_data = self.get_set(set)
            if "tokens" not in set_data.keys():
                return

            for c in set_data["tokens"]:
                if c["uuid"]==uuid:
                    #print("Got card: "+c["name"])
                    card = Card(json_dict=c)
                    card.set = set
                    card.isToken=True
                    return card

        if not only_that_set:
            print("Token "+uuid+" not found")
        else:
            print("Token "+uuid+" not found in set "+set_code)
        return None


    def weighted_card_selection(card_set, n=1):
        """include weights as card attributes"""
        if len(card_set)==1:
            return card_set
        return random.choices(card_set, [card.current_weight for card in card_set], k=n)

    def generate_booster(self, set_code, booster_type="default", n=1):
        # First get the booster sheet amounts. More info here: https://mtgjson.com/abstract-models/booster/

        set = self.get_set(set_code)
        if "booster" not in set.keys():
            print("No boosters found for set "+set_code)
            return None

        if booster_type not in set["booster"].keys() and booster_type != "default":
            print("'"+booster_type+"' booster not found in set "+set_code+" - default used instead")
            booster_type="default"
            if "default" not in set["booster"].keys():
                print("'"+booster_type+"' booster not found in set "+set_code)
                return

        tr = melt_dicts(set["booster"][booster_type]["boosters"], "contents", "weight")

        contents = tr["contents"]
        weights = tr["weight"]

        boosters_distrib = random.choices(contents, weights=weights, k=n)

        cards=[]

        # Now we have a list of all the boosters to be output and the sheets they draw from.
        # Next we need to iterate over them, selecting from sheets
        # But we'll also need to allow for color balancing!
        # So let's convert the UUIDs to Cards for each booster, and sort by color

        sheets_raw=set["booster"][booster_type]["sheets"]
        sheets_processed = {}
        sheets_color_balance = {}
        sheets_color_split = {}

        for sheet, sheet_comp in sheets_raw.items():
            sheets_processed[sheet] = []
            sheets_color_balance[sheet] = (True if sheet_comp.get("balanceColors")==True else False)

            if sheets_color_balance[sheet]:
                # gotta balance colors for this sheet, so lets split by color too
                # basically just set up the list for it
                sheets_color_split[sheet] = {}
                for color in Mtgjson.COLORS:
                    sheets_color_split[sheet][color] = []
                sheets_color_split[sheet]["c"] = []

            # get all cards at once, will be quicker
            sheet_cards=self.get_cards_by_uuids(sheet_comp["cards"].keys())
            if len(sheet_cards)<len(sheet_comp["cards"].keys()):
                return

            # now lets iterate over each card in the sheet
            for card in sheet_cards:
                card.current_sheet=sheet
                card.current_weight=sheet_comp["cards"][card.uuid]
                sheets_processed[sheet] += [card]
                if sheets_color_balance[sheet]:
                    if card.colorIdentity == []:
                        sheets_color_split[sheet]["c"] += [card]
                    else:
                        for color in Mtgjson.COLORS:
                            if color in card.colorIdentity:
                                sheets_color_split[sheet][color] += [card]

            #print("sheet",sheet,"containing",len(sheet_comp["cards"]),"cards")

        # Okay! now sheets_processed is a dict where the keys are sheet names and
        # the values the lists of cards, and each card has its weight stored internally.
        # As well, sheets_color_balance is a similar dict but values are booleans
        # Shows which ones need to be color balanced
        # and those ones are also split into colors in sheets_color_split

        # wow this is a lot of work

        # now we need to go through each booster, and each sheet, and pick Cards

        booster_cards = []

        for booster in boosters_distrib: # for each booster requested
            for sheet, amount in booster.items(): # for each sheet listed for the booster
                current_sheet_cards = []
                if sheets_color_balance[sheet]:

                    for color in Mtgjson.COLORS:
                        card = Mtgjson.weighted_card_selection(sheets_color_split[sheet][color])[0]
                        while card in current_sheet_cards:
                            card = Mtgjson.weighted_card_selection(sheets_color_split[sheet][color])[0]
                        current_sheet_cards += [card]

                    if amount < 5:
                        print("WARNING: Added too many cards to booster! Should have been",amount)
                    # then do the weird allocation thing for the rest
                    n = len(sheets_processed[sheet])
                    color_weights = { "W": len(sheets_color_split[sheet]["W"]) * amount - n,
                                      "B": len(sheets_color_split[sheet]["B"]) * amount - n,
                                      "U": len(sheets_color_split[sheet]["U"]) * amount - n,
                                      "R": len(sheets_color_split[sheet]["R"]) * amount - n,
                                      "G": len(sheets_color_split[sheet]["G"]) * amount - n,
                                      "c": len(sheets_color_split[sheet]["c"]) * amount
                                    }

                    while len(current_sheet_cards) < amount:
                        #add at random with color weights
                        # select color first..
                        colors = Mtgjson.COLORS+["c"]

                        c_weights = [color_weights[c] for c in colors]

                        color = random.choices(colors, weights=c_weights)[0]
                        # now lets grab a weighted random card from this selection
                        card = Mtgjson.weighted_card_selection(sheets_color_split[sheet][color])[0]
                        if len(sheets_color_split[sheet][color])>1:
                            while card in current_sheet_cards:
                                card = Mtgjson.weighted_card_selection(sheets_color_split[sheet][color])[0]
                        current_sheet_cards += [card]

                else:
                    # No color balance, just grab cards from sheet willy-nilly
                    while len(current_sheet_cards) < amount:
                        card = Mtgjson.weighted_card_selection(sheets_processed[sheet])[0]
                        while card in current_sheet_cards:
                            card = Mtgjson.weighted_card_selection(sheets_processed[sheet])[0]
                        current_sheet_cards += [card]

                booster_cards += current_sheet_cards

        # Return list of Cards

        return booster_cards


    def booster_analysis(self, set_code, n=1000):

        rares = 0
        mythics = 0
        colors = Mtgjson.COLORS + ["c"]

        color_amts = {}
        for c in colors:
            color_amts[c] = 0

        card_amt = 0
        start = time.time()
        for i in range(n):
            b = self.generate_booster(set_code)
            for c in b:
                card_amt +=1
                #print(c,c.colorIdentity)
                if c.rarity =="mythic":
                    mythics += 1
                elif c.rarity == "rare":
                    rares += 1
                if len(c.colorIdentity) == 0:
                    color_amts["c"] += 1
                else:
                    for color in colors:
                        if color in c.colorIdentity:
                            color_amts[color]+=1

        t = time.time()-start
        print("time taken to open",n,set_code.upper(),"boosters:",round(t*1000,1),"ms ("+str(round(t*1000/n,1)),"ms per booster)")
        print("total cards:",card_amt)

        print("rares:",rares)
        print("mythics:",mythics)
        if mythics != 0:
            print("ratio:  1:"+str( float(rares)/float(mythics)))
        print("Color breakdown")

        for color in colors:
            print(color+":", color_amts[color],"("+str(int(color_amts[color]*100/float(card_amt)))+" %)")


    def booster_print(self, set_code, booster_type="default"):
        set = self.get_set(set_code)
        for booster in set["booster"][booster_type]["boosters"]:
            print("--Booster config--")
            for sheet, amount in booster["contents"].items():
                print(amount,"cards from sheet '"+sheet+"'")


    def sheet_print(self, set_code, sheet_name, booster_type="default"):
        set = self.get_set(set_code)
        print("Sheet",sheet_name,"from",set_code,"contains:")
        for card,weight in set["booster"][booster_type]["sheets"][sheet_name]["cards"].items():
            print(self.get_card_by_uuid(card),"(weight",str(weight)+")")

    def make_draft_pool(self, boosters):
        pool = []
        for set_code, amount in boosters.items():
            booster = self.generate_booster(set_code, n=amount)
            if booster is not None:
                pool += booster
        return pool


class Card(object):
    """docstring for Card."""

    def __init__(self, json_dict = None):
        super(Card, self).__init__()
        self.artist = None
        self.availability = None
        self.borderColor = None
        self.colorIdentity = None
        self.colors = None
        self.convertedManaCost = None
        self.edhrecRank = None
        self.finishes = None
        self.flavorText = None
        self.foreignData = None
        self.frameVersion = None
        self.hasFoil = None
        self.hasNonFoil = None
        self.identifiers = None
        self.keywords = None
        self.layout = None
        self.legalities = None
        self.manaCost = None
        self.manaValue = None
        self.name = None
        self.number = None
        self.originalText = None
        self.originalType = None
        self.power = None
        self.printings = None
        self.purchaseUrls = None
        self.rarity = None
        self.rulings = None
        self.setCode = None
        self.subtypes = None
        self.supertypes = None
        self.text = None
        self.toughness = None
        self.type = None
        self.types = None
        self.uuid = None
        if json_dict is not None:
            self.get_from_dict(json_dict)

        self.current_weight = -1
        self.current_sheet = None
        self.set = None
        self.isToken = False
        self.image = None

    def get_from_dict(self, card_json_dict):
        self.artist = card_json_dict.get("artist")
        self.availability = card_json_dict.get("availability")
        self.borderColor = card_json_dict.get("borderColor")
        self.colorIdentity = card_json_dict.get("colorIdentity")
        self.colors = card_json_dict.get("colors")
        self.convertedManaCost = card_json_dict.get("convertedManaCost")
        self.edhrecRank = card_json_dict.get("edhrecRank")
        self.finishes = card_json_dict.get("finishes")
        self.flavorText = card_json_dict.get("flavorText")
        self.foreignData = card_json_dict.get("foreignData")
        self.frameVersion = card_json_dict.get("frameVersion")
        self.hasFoil = card_json_dict.get("hasFoil")
        self.hasNonFoil = card_json_dict.get("hasNonFoil")
        self.identifiers = card_json_dict.get("identifiers")
        self.keywords = card_json_dict.get("keywords")
        self.layout = card_json_dict.get("layout")
        self.legalities = card_json_dict.get("legalities")
        self.manaCost = card_json_dict.get("manaCost")
        self.manaValue = card_json_dict.get("manaValue")
        self.name = card_json_dict.get("name")
        self.number = card_json_dict.get("number")
        self.originalText = card_json_dict.get("originalText")
        self.originalType = card_json_dict.get("originalType")
        self.power = card_json_dict.get("power")
        self.printings = card_json_dict.get("printings")
        self.purchaseUrls = card_json_dict.get("purchaseUrls")
        self.rarity = card_json_dict.get("rarity")
        self.rulings = card_json_dict.get("rulings")
        self.setCode = card_json_dict.get("setCode")
        self.subtypes = card_json_dict.get("subtypes")
        self.supertypes = card_json_dict.get("supertypes")
        self.text = card_json_dict.get("text")
        self.toughness = card_json_dict.get("toughness")
        self.type = card_json_dict.get("type")
        self.types = card_json_dict.get("types")
        self.uuid = card_json_dict.get("uuid")

    def get(self,varname):
        return getattr(self,varname)

    def __str__(self):
        return self.name

    def __repr__(self):
        return "<Card '"+self.name+"' "+str(self.colorIdentity)+" "+str(self.rarity)+">"

    def __eq__(self, other):
        if (isinstance(other, Card)):
            return self.uuid == other.uuid

    def sort(pool, sort_attribute, reverse=True):
        not_none_list = []
        none_list = []

        for card in pool:
            if card.get(sort_attribute) is not None:
                not_none_list += [card]
            else:
                none_list += [card]

        not_none_list.sort(key=lambda x: x.get(sort_attribute), reverse=reverse)
        return not_none_list+none_list

class Trie(object):
    """docstring for Trie."""

    def __init__(self, is_case_sensitive = False):
        super(Trie, self).__init__()
        self.tree = {}
        self.is_case_sensitive = is_case_sensitive

    def add_entry(self, key, value):
        node = self.tree
        key = key.lower() if not self.is_case_sensitive else key
        for char in key:
            if char not in node:
                node[char] = {}
            node = node[char]
        if "leaf" not in node:
            node["leaf"] = []
        node["leaf"] += [value]

    def _get_node(self, key):
        node = self.tree
        for char in key:
            if char in node:
                node = node[char]
            else:
                return None
        return node

    def get_entry(self, key):
        key = key.lower() if not self.is_case_sensitive else key
        return self._get_node(key)["leaf"]

    def _collect(self, node, current_str = ""):
        keys = []
        for key, value in node.items():
            if len(key) == 1 and type(value) is dict:
                keys += [key+c for c in self._collect(value, current_str+key)]
            elif key != "leaf":
                return key
        return keys

    def get_keys_with_prefix(self, prefix):
        node = self._get_node(prefix)
        if node is None:
            return []
        suffixes = self._collect(node)
        return [prefix + s for s in suffixes]

    def _print_tree(self, node, level):
        s = ("|  ")*level
        for key, value in node.items():
            if type(value) is dict: #goin down a level
                print (s + key)
                print (s + "|__")
                self._print_tree(value, level+1)
            else: #end of a node, print output
                print (s + key +"="+str(value))


    def print_tree(self):
        self._print_tree(self.tree, 0)

#    def __delitem__(self, key):
#        pass #shit man idk

    def __getitem__(self, key):
        return self.get_entry(key)

    def __setitem__(self, key, value):
        self.add_entry(key, value)


#def save_object(object, filename):
#    with open(filename, "wb") as outfile:
#        pickle.dump(object, outfile, pickle.HIGHEST_PROTOCOL)
#
#
#def load_object(filename):
#    with open(filename, "rb") as infile:
#        return pickle.load(infile)
def main():
    mtg = Mtgjson()

    card_list = mtg.get_cards_by_name("Murder")
    for card in card_list:
        if card.flavorText is not None:
            print(card.set+": "+card.flavorText)

if __name__ == '__main__':
    main()
